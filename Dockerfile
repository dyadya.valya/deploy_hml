FROM python:3.8-buster

WORKDIR /app
COPY . .
ARG USER_GID=$USER_GID
ARG USER_UID=$USER_UID
ARG USERNAME=$USERNAME
ARG SECRET_NUMBER

ENV secret_number=$SECRET_NUMBER
ENV PYTHONPATH="/app"
RUN pip install flask requests redis uwsgi
CMD uwsgi --http 0.0.0.0:9090 --wsgi-file app.py --master --processes 1 --threads 1
