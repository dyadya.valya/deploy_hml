import os
import sys 
import signal
from time import sleep
import requests
from flask import Flask, jsonify
import redis

secret_number = os.environ['secret_number']

app = Flask(__name__)

@app.route('/return_secret_number', methods=['GET', 'POST'])
def hello():
    sleep(1)
    return jsonify({'secret_number': secret_number})

if __name__ == "__main__":
    app.run()
else:
    application = app
